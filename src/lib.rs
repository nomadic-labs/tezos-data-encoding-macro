use proc_macro::TokenStream;

enum IntsEncoding {
    Int8,
    UInt8,
    Int16,
    UInt16,
    Int31,
    Int32,
    Int64,
}

fn parse_tokens(token_stream: TokenStream) -> IntsEncoding {
    let mut token_iter = token_stream.into_iter().peekable();
    match token_iter.next() {
        Some(proc_macro::TokenTree::Literal(lit)) => {
            let stringlit = lit.to_string();
            if stringlit.eq("int8") {
                return IntsEncoding::Int8;
            } else {
                panic!("unknown token")
            }
        }
        Some(_) => {
            panic!("unknown token")
        }
        None => {
            panic!("no token")
        }
    }
}

fn code_of_encoding(encoding: IntsEncoding) -> TokenStream {
    match encoding {
        IntsEncoding::Int8 => {
            return TokenStream::new();
        }
        _ => {
            panic!("unsupported encoding")
        }
    }
}

#[proc_macro]
pub fn encoding(input: TokenStream) -> TokenStream {
    let encoding = parse_tokens(input);
    let code = code_of_encoding(encoding);
    return code;
}
